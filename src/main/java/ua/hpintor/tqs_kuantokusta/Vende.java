/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.hpintor.tqs_kuantokusta;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hrcpi
 */
@Entity
@Table(name = "vende")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vende.findAll", query = "SELECT v FROM Vende v")
    , @NamedQuery(name = "Vende.findById", query = "SELECT v FROM Vende v WHERE v.id = :id")
    , @NamedQuery(name = "Vende.findByPreco", query = "SELECT v FROM Vende v WHERE v.preco = :preco")
    , @NamedQuery(name = "Vende.findByPrecoKg", query = "SELECT v FROM Vende v WHERE v.precoKg = :precoKg")
    , @NamedQuery(name = "Vende.findByDesconto", query = "SELECT v FROM Vende v WHERE v.desconto = :desconto")})
public class Vende implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "preco")
    private Double preco;
    @Column(name = "preco_kg")
    private Double precoKg;
    @Column(name = "desconto")
    private Integer desconto;
    @JoinColumn(name = "id_produto", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Produto idProduto;
    @JoinColumn(name = "id_super_mercado", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private SuperMercado idSuperMercado;

    public Vende() {
    }

    public Vende(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public Double getPrecoKg() {
        return precoKg;
    }

    public void setPrecoKg(Double precoKg) {
        this.precoKg = precoKg;
    }

    public Integer getDesconto() {
        return desconto;
    }

    public void setDesconto(Integer desconto) {
        this.desconto = desconto;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    public SuperMercado getIdSuperMercado() {
        return idSuperMercado;
    }

    public void setIdSuperMercado(SuperMercado idSuperMercado) {
        this.idSuperMercado = idSuperMercado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vende)) {
            return false;
        }
        Vende other = (Vende) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ua.hpintor.tqs_kuantokusta.Vende[ id=" + id + " ]";
    }
    
}
