/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.hpintor.tqs_kuantokusta;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hrcpi
 */
@Entity
@Table(name = "super_mercado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SuperMercado.findAll", query = "SELECT s FROM SuperMercado s")
    , @NamedQuery(name = "SuperMercado.findById", query = "SELECT s FROM SuperMercado s WHERE s.id = :id")
    , @NamedQuery(name = "SuperMercado.findByNome", query = "SELECT s FROM SuperMercado s WHERE s.nome = :nome")
    , @NamedQuery(name = "SuperMercado.findByLocaliza\u00e7\u00e3o", query = "SELECT s FROM SuperMercado s WHERE s.localiza\u00e7\u00e3o = :localiza\u00e7\u00e3o")})
public class SuperMercado implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSupermercado")
    private Collection<Fornecedor> fornecedorCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "localiza\u00e7\u00e3o")
    private String localização;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSuperMercado")
    private Collection<Vende> vendeCollection;

    public SuperMercado() {
    }

    public SuperMercado(Integer id) {
        this.id = id;
    }

    public SuperMercado(Integer id, String nome, String localização) {
        this.id = id;
        this.nome = nome;
        this.localização = localização;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLocalização() {
        return localização;
    }

    public void setLocalização(String localização) {
        this.localização = localização;
    }

    @XmlTransient
    public Collection<Vende> getVendeCollection() {
        return vendeCollection;
    }

    public void setVendeCollection(Collection<Vende> vendeCollection) {
        this.vendeCollection = vendeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuperMercado)) {
            return false;
        }
        SuperMercado other = (SuperMercado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ua.hpintor.tqs_kuantokusta.SuperMercado[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<Fornecedor> getFornecedorCollection() {
        return fornecedorCollection;
    }

    public void setFornecedorCollection(Collection<Fornecedor> fornecedorCollection) {
        this.fornecedorCollection = fornecedorCollection;
    }
    
}
